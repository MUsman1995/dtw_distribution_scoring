import pandas as pd
import numpy as np
import math
import scipy
from datetime import datetime, timedelta
from senseye_data_api import SenseyeAPI
from . get_wake_time import get_wake_time
from . prepare_sleep_responses import prepare_sleep_responses
from . sleep_modeling import sleep_modeling


class BodyBatteryCalculation():
    '''Calculate "body battery" from reported hours of sleep. Will check to see
    if dataset has the 'wake_time' column, queries Senseyse Data API for it if
    not present.

    Attributes:
        df (pandas dataframe): single dataset with merged fatigue survey responses
        sleep_col (string): name of column containing 24 hour sleep response

    NOTE: This function is meant to run on a single dataset. This function can
    easily be put into a loop to run through a stacked dataframe by sorting by
    user_session_id.

    '''
    def __init__(self, df):
        # initialize variables
        self.df = df

    def run(self):
        # check if dataframe has the 'wake_time' column, if not, add it
        if not "wake_time" in self.df.columns:
            self.df['wake_time'] = get_wake_time(self.df['user_session_id'].iloc[0])

        # convert to datetime, calculate time awake
        self.df = prepare_sleep_responses(self.df)
        # do sleep modeling to calculate body battery
        self.df = sleep_modeling(self.df)
        return self.df
