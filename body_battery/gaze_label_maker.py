import pandas as pd
import numpy as np
from core_feature_requests.transforms import apply_filter_to_data

def gaze_label_maker(original_data, medfiltval = 9, blk_thresh = 7, sacc_thresh = 1, fix_thresh = 0):
    '''detects saccade and fixation and pursuit from position. <NOTE: This
    function DOES NOT use remodnav>. This function needs interpolated data from
    eyecleaningfunction

    Args:
        original_data (pandas dataframe): dataset with columns called
        pupil_center_right_x_interpolated and pupil_center_left_x_interpolated
        and user_session_id



    Kwargs:
        medfiltval (int):(default:9) Median filter window size. Must be an odd number. Increase the
        number if you want more filter (will soomth out the data), while lower
        number will keep it as similar to the raw data as possible. For OKR,
        suggest using 9; while for nystagmus and smooth Pursuit, use 31.

        blk_thresh (int): (default = 7) high end of blink threshold for velocity

        sacc_thresh (int): (default = 1) saccade threshold

        fix_thresh (int): (default = 0) fixation threshold

    Returns:
        df_all (pandas dataframe): dataset which is Originaldata with additional
        columns with gaze labeled called 'Gaze_label_left' and 'Gaze_label_left'
        filtered both eye data called 'dataL' and 'dataR' and velocity of
        pupil x left position called 'testdiffL' and 'testdiffR'
    '''
    df_1 = original_data


    eye = 'both'


    [datarawX2OR, datarawX2OL] = apply_filter_to_data(df_1,medfiltval, eye)

    df_1['dataL']  = datarawX2OL
    df_1['dataR']  = datarawX2OR
    df_1['testdiff_L'] = df_1['dataL']
    df_1['testdiff_L'][0] = 0
    df_1['testdiff_L'][1:] = np.diff(df_1['dataL'])
    df_1['testdiff_R'] = df_1['dataR']
    df_1['testdiff_R'][0] = 0
    df_1['testdiff_R'][1:] = np.diff(df_1['dataR'])

    conditionsL = [
        (abs(df_1['testdiff_L']) >= sacc_thresh) &  (abs(df_1['testdiff_L']) < blk_thresh),
        (abs(df_1['testdiff_L']) < sacc_thresh) & (abs(df_1['testdiff_L']) > fix_thresh), abs(df_1['testdiff_L']) == fix_thresh
    ]


    # choices = ['sacc', 'purs','fix']
    choices = [int(1), int(2), int(3)]
    df_1['Gaze_label_left'] = np.select(conditionsL, choices, default = int(0))

    conditionsR = [
        (abs(df_1['testdiff_R']) >= sacc_thresh) &  (abs(df_1['testdiff_R']) < blk_thresh),
        (abs(df_1['testdiff_R']) < sacc_thresh) & (abs(df_1['testdiff_R']) > fix_thresh), abs(df_1['testdiff_R']) == fix_thresh
    ]

    df_1['Gaze_label_right'] = np.select(conditionsR, choices, default = int(0))


    return df_1
