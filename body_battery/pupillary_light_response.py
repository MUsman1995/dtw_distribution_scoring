import pandas as pd


class PupillaryLightResponse():
    '''Quantifies the pupillary light response by returning various stats about how the pupil behaved
    over the time course of the pupil response.

    Attributes:
        df (pandas dataframe): dataframe with unet mask output columns
        start (float): start time of pupil constriction
        end (float): end time of pupil constriction
        latency (float): length of time before pupil starts constricting following white screen start
        velocity (float): velocity of pupil constriction during the period between start and end
        amplitude (float): change in pupil area between start and end
        perc_amp (float): percent change in pupil area relative to start area

    NOTE:
        All internal functions are using internal class attributes.
    '''
    def __init__(self, df, start=None, end=None,
                 latency=None, velocity=None,
                 amplitude=None, perc_amp=None):

        self.df = df
        self.start = start
        self.end = end
        self.latency = latency
        self.velocity = velocity
        self.amplitude = amplitude
        self.perc_amp = perc_amp

    def prepdf(self):
        """Prepares the dataframe for analysis by dropping irrelevant rows.

        """
        self.df = self.df[(self.df['type'] == 'luminance_start')|(self.df['type'] == 'luminance')|(self.df['type'] == 'luminance_stop')]
        self.df = self.df[self.df['color'] == 255]

    def find_constriction_start(self):
        """Identifies the timestamp when the pupil began constricting.

        """
        self.df['cum_min'] = self.df['pupil_area_right'].cummin()
        self.df['cum_min_rolling'] = self.df['cum_min'].rolling(10).mean().diff()**2
        self.df['cum_min_small'] = self.df['cum_min'].rolling(10).mean().diff()
        self.df['diff_8'] = self.df['cum_min_rolling'] - self.df['cum_min_small']
        sf = self.df[self.df['diff_8'] > 1.2 ]
        # min latency of 180 ms
        sf = sf[sf['timestamp']> self.df['timestamp'].iloc[0]+0.18]
        self.start = (sf['timestamp'].iloc[0])

    def find_constriction_end(self):
        """Identifies the timestamp when the pupil stops constricting and
        reaches its minimum area.

        """

        const = self.df[self.df['timestamp']>= self.start].iloc[40:]
        const['cum_min'] = const['pupil_area_right'].cummin()
        const['cum_min_rolling'] = const['cum_min'].rolling(10).mean().diff()**2
        const['cum_min_small'] = const['cum_min'].rolling(10).mean().diff()
        const['diff_8'] = const['cum_min_rolling'] - const['cum_min_small']
        sf = const[const['diff_8'] <= 5]
        self.end = (sf['timestamp'].iloc[0])

    def calculate_latency(self):
        """Determines the latency for the pupil to begin constricting
        following the start of the stimulus.

        """
        stim_on = self.df['timestamp'].iloc[0]
        self.latency = self.start - stim_on


    def calculate_constriction_velocity(self):
        """Calculates the velocity of the pupillary constriction event.

        """
        # find constriction start size
        start_size = self.df[self.df['timestamp'] == self.start]['pupil_area_right'].iloc[0]
        # find constriction end size
        end_size = self.df[self.df['timestamp'] == self.end]['pupil_area_right'].iloc[0]
        # calculate velocity: change in size over change in time.
        self.velocity = (end_size - start_size)/(self.end-self.start)
        # calculate amplitude: max size - min size
        self.amplitude = start_size - end_size
        # calculate percent amplitude: change in pupi size relative to start size
        self.perc_amp = ((start_size - end_size)/start_size)*100



    # wrapper
    def run(self):
        """Runs the previous functions in the correct order.

        Returns:
            Tuple: start, end, latency, velocity, amplitude, perc_amp

        """
        # subset to luminance calibration
        self.prepdf()
        # find constriction start
        self.find_constriction_start()
        # find constriction end
        self.find_constriction_end()
        # find constriction latency
        self.calculate_latency()
        # find constriction velocity, amplitude, and percent amplitude
        self.calculate_constriction_velocity()

        return self.start, self.end, self.latency, self.velocity, self.amplitude, self.perc_amp
