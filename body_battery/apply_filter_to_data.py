
import numpy as np
import scipy.signal

def apply_filter_to_data(df, medfiltval= 9, eye = 'both'):

    data_xR = np.array(df.pupil_centers_right_x_interpolated)
    data_xL = np.array(df.pupil_centers_left_x_interpolated)

    datarawXOL = data_xL[:round(len(data_xL))] # make sure left and right eye is the same length
    datarawXOR = data_xR[:round(len(data_xR))]

    datarawX2OL = scipy.signal.medfilt(datarawXOL, kernel_size = medfiltval)
    datarawX2OR = scipy.signal.medfilt(datarawXOR, kernel_size = medfiltval)

    datarawX2OL = scipy.signal.savgol_filter(datarawX2OL, 3, 2)
    datarawX2OR = scipy.signal.savgol_filter(datarawX2OR, 3, 2)
    FilterLeft = datarawX2OL
    FilterRight = datarawX2OR

    if eye == 'right':
        return FilterRight
    elif eye == 'left':
        return FilterLeft
    elif eye == 'both':
        return FilterRight, FilterLeft
    else:
        print("error")
