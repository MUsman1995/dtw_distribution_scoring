import pandas as pd
import numpy as np
import math


def sleep_modeling(df, sleep_col='24_hour_sleep', k = 0.5*60,
                   a_s = 0.235, tau_d = 4.2, p = 18, p_prime = 3, beta = 0.5, Rc = 2880,
                   a_1 = 7, a_2 = 5, I_max = 5, f =  0.0026564*60, i = 0.04):
    """Make predictions on cognitive perfomance based on hours slept/time awake.
    Adapted from Peng et al. https://www.ncbi.nlm.nih.gov/pubmed/29297761

    Kwargs:
            sleep_col (string): name of column with amount of sleep
            k (float): sleep depletion rate, 0.5 units/min
            a_s (float): conversion factor, 0.235
            tau_d (float): sleep recovery time constant, 4.2 hours
            p (int): peak of 24 hour circadian rhythm, 18 hours
            p_prime (int): peak of 12 hour circadian rhythm, 3 hours
            beta (float): weighting factor, 0.5
            Rc (int): reservoir sleep capacity, 2880 units
            a_1 (int): constant, 7%
            a_2 (int): constant, 5%
            I_max (float): maximal intertial effect of awakening, 5%
            f (float): constant, 0.0026564*60
            i (float): inertial time constant, 0.04

        Args:
            df (dataframe): df with a sleep column and time_awake column

        Returns:
            df (dataframe): same dataframe, but with added column:
                'body_battery': predicted task effectivness, expressed as percentage


    """

    # recovery of sleep
    df['R_t_sleep'] = 2400*(a_s+1)*(1-np.exp(-(df[sleep_col].astype('float'))/tau_d))
    df['R_t_awake'] = df['R_t_sleep'] - k*df['time_awake']

    # circadian influence
    df['C_t'] = [math.cos(2*math.pi*((x-p)/24)) + beta*math.cos(4*math.pi*((x-p-p_prime)/24)) for x in df['time_awake']]

    # # sleep inertia
    df['I_t_1'] = [-I_max*np.exp(-(i*x)) for x in df['time_awake']]
    df['I_t_2'] = [-a_s*y for y in df['C_t']]
    df['I_t_3'] = [f*(Rc-y) for y in df['R_t_awake']]

    df['I_t_d'] = df['I_t_2'] + df['I_t_3']
    df['I_t'] = df['I_t_1']/df['I_t_d']

    # predicted energy percentage
    df['body_battery'] = abs(100*(df['R_t_awake']/Rc) + (a_1 + a_2*(Rc-df['R_t_awake'])/Rc)*df['C_t']+df['I_t'])

    # drop working columns
    df = df.drop(['R_t_sleep', 'R_t_awake', 'C_t', 'I_t_1',
                            'I_t_2', 'I_t_3', 'I_t_d', 'I_t'], axis = 1)

    return(df)
