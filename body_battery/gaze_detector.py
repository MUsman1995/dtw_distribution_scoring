import remodnav as rm
import pandas as pd

def gaze_detector(data,px2deg = 0.5, sr = 300):
    '''detects saccade and fixation and pursuit from position

    Args:
        data (pandas dataframe): column names needs to be x and y positions.
    Kwargs:
        px2deg (float): pixel per degrees, default 0.5
        sir (int): framerate the data is collected, default 300
    Returns:
        pandas dataframe: gaze label (expect 11 columns)
    '''    

    clf = rm.EyegazeClassifier(
                    px2deg=px2deg,
                    sampling_rate=sr,
                    pursuit_velthresh=5.,
                    noise_factor=3.0,
                    lowpass_cutoff_freq=10.0,
                )


    p = clf.preproc(data)
    events = clf(p)
    events = pd.DataFrame(events)
    saccades = events[events['label'] == 'SACC']
    isaccades = events[events['label'] == 'ISAC']
    hvpso = events[(events['label'] == 'HPSO') | (events['label'] == 'IHPS')]
    lvpso = events[(events['label'] == 'LPSO') | (events['label'] == 'ILPS')]
    del events['id']
    return events
