import pandas as pd
import numpy as np
from datetime import datetime, timedelta


def prepare_sleep_responses(df):
    """Convert responses into datetime objects and calculate time awake. Put it into the dataframe.
    Args:
        df (dataframe): experiment dataframe with orm survey data

    Returns:
        df (dataframe): same dataframe, but with additional columns:
            'experiment_start': timestamp of first frame
            'time_of_wake': datatime object of survey response when they woke up
            'time_awake': how long they have been awake, in hours
    """
    experiment_start = datetime.utcfromtimestamp(df['timestamp'].iloc[0]).strftime('%I:%M:%S %p')
    df['experiment_start'] = datetime.strptime(experiment_start, '%I:%M:%S %p')
    df['time_of_wake'] = datetime.strptime(df['wake_time'].iloc[0], '%I:%M %p')

    # if they got zero hours of sleep, subtract a day from time_of_wake
    df.loc[df['24_hour_sleep'] == 0, 'time_of_wake'] = df.loc[df['24_hour_sleep'] == 0, 'time_of_wake'] - timedelta(days=1)

    # calculate difference, convert to hours
    df['time_awake'] = df['experiment_start'] -df['time_of_wake']
    df['time_awake'] = [x.total_seconds()/3600 for x in df['time_awake']]

    # if time_awake is negative, it means they woke up YESTERDAY and have been awake since. Subtract a day from time_of_wake
    df.loc[df['time_awake'] <= 0, 'time_of_wake'] = df.loc[df['time_awake'] <= 0, 'time_of_wake'] - timedelta(days=1)
    df['time_awake'] = df['experiment_start'] -df['time_of_wake']
    df['time_awake'] = [x.total_seconds()/3600 for x in df['time_awake']]

    return(df)
