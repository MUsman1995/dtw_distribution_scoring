import pandas as pd
import numpy as np


def one_runs(event_array):
    """Get length of consecutive runs of ones. Used in gaze quantification.

        Args:
            event_array (array): binary array
        Returns:
            ranges (array): 2D array containing start and end of events
    """
    iszero = np.concatenate(([0], event_array, [0]))
    absdiff = np.abs(np.diff(iszero))
    # Runs start and end where absdiff is 1.
    ranges = np.where(absdiff == 1)[0].reshape(-1, 2)
    return ranges
