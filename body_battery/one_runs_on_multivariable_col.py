import pandas as pd
from core_feature_requests.transforms import one_runs

def one_runs_on_multivariable_col(df, col_name):
    """Get length of consecutive runs of ones when the label is non binary.
    <Note: requires one_runs.py>

        Args:
            df (panda dataset): dataset that contains col_name

            col_name (string): column name of the gaze label (generated from
            gaze_label_maker.py). If you used gaze_label_maker.py, the default
            column name for the label should be 'Gaze_label_left' or
            'Gaze_label_right'. pick which eye you want to analyze. usually
            left eye.

        Returns:
            len_df (panda dataframe): dataframe containing start and end of
            events as well as label of the events. saccade = 1, pursuit = 2,
            fixation = 3
        """
    dummies = pd.get_dummies(df[col_name].values)
    len_df = pd.DataFrame()
    for item in df[col_name].unique():
        r = one_runs(dummies[int(item)])
        r_df = pd.DataFrame(r, columns = ['start', 'end'])
        r_df['label'] = int(item)
        len_df = len_df.append(r_df)
#         print(item)
    len_df.reset_index(drop=True, inplace=True)
    return len_df
