import pandas as pd
import numpy as np
from senseye_data_api import SenseyeAPI

def get_wake_time(user_session_id):
    """Use dataset information to get user_session_id and query for participant wake time.

    Args:
        user_session_id (string): session id

    Returns:
        wake_time (string): hours/min time of day from orm surveys
        """
    api = SenseyeAPI(context = None, url='https://api.senseye.dev')
    session = api.get('sessions/' + user_session_id)
    for survey in session['surveys']:
        if survey['type'] == 'orm': # orm survey entry has what is needed
            wake_time = survey['responses']['wake_time']

    return(wake_time)
