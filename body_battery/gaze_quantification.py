import pandas as pd
import numpy as np
from core_feature_requests.transforms import apply_filter_to_data
from core_feature_requests.transforms import one_runs_on_multivariable_col

def gaze_quantification(df):
    '''quantifies the gaze labeled data. This function needs interpolated data from
    eyecleaningfunction as well as gaze labeled data from gaze_label_maker.py.

    Args:
        df (pandas dataframe): dataset with columns called gaze_label_right or
        gaze_label_left

    Returns:
        okr_df (pandas dataframe): dataset that containst start and end of each
        labels (fixation = 3, saccade = 1, or pursuit = 2) as well as duration and
        slope (velocity) when the label occurs. the data frame also contains columns
        that are useful in analysis that were pulled from original df (BAC,
        24 hour sleep, dayid, body battery, fatigue, PVT performance etc.)
    '''
    okr_df = []
    okr_df = pd.DataFrame(okr_df)

    okr_df = one_runs_on_multivariable_col(df, 'Gaze_label_left')

    a_start = np.array(okr_df['start'])
    a_end = np.array(okr_df['end'])
    a_pupil_pos = np.array(df['dataL'])
    a_amp = (a_pupil_pos[a_end-1] - a_pupil_pos[a_start-1])
    a_dur = (a_end - a_start)

    okr_df['duration'] = a_dur
    okr_df['slope'] = a_amp/a_dur
    okr_df['BAC'] = df['BAC'].iloc[1]
    okr_df['BAC'] = df.BAC.iloc[1]
    okr_df['fatigue'] = df.fatigue.iloc[1]
    okr_df['24_hour_sleep'] = df['24_hour_sleep'].iloc[1]
    # TODO; checkup on if we are able to include this in the pipeline in the future for predictions
    # print('body_battery' in df.columns.values)
    # okr_df['body_battery'] = df['body_battery'].iloc[1]
    # okr_df['raw_pvt_performance'] = df['raw_pvt_performance'].iloc[1]
    # okr_df['population_pvt_performance'] = df['population_pvt_performance'].iloc[1]
    # okr_df['individual_pvt_performance'] = df['individual_pvt_performance'].iloc[1]
    okr_df['dayid'] = df.dayid.iloc[1]

    return okr_df
